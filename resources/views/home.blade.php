@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                    <br/><br/>
                    <form method="post" action="/tasks">
                        @csrf
                        <input type="text" name="task" placeholder="Add task">
                        <input type="hidden" name="status" value="pending">
                        <button type="submit" class="bg-secondary text-white">Submit</button>
                    </form>

                    <br/>
                    @if(count($posts) > 0)
                     <table class="table table-header">
                            <thead>
                                <tr>
                                    <th>Task</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                           </thead>
                           <tbody>
                                @foreach($tasks as $task)
                               <tr>
                                   <td>{{ $task->name }}</td>
                                   <td>{{ $task->status }}</td>
                               </tr>
                               @endforeach
                           </tbody>
                     </table>
                     @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
