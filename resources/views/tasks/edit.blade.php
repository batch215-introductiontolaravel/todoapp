@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <h1>Edit Task</h1>
    <form method="post" action="/tasks/{{$task->id}}">
        @method('PUT')
        @csrf
        <div class="form-group">
                <label for="name">Task Name</label>
                <input class="form-control" placeholder="name" name="name" type="text" id="name" value="{{ $task->name }}">
        </div>

        <div class="form-group">
                <label for="status">Status</label>
                <textarea class="form-control" placeholder="status" name="status" rows="10" id="status">{{ $task->status }}</textarea>
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Update</button>
    </form>

    <script>
ClassicEditor
.create( document.querySelector( '#status' ) )
.catch( error => {
console.error( error );
} );
</script>
@endsection
